import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AwsRequestMaker {
    public static Map<String, String> getCognitoHeaders(String body) {
        Map<String, String> headerProperties = new HashMap<>();
        headerProperties.put("X-Amz-Target", "AWSCognitoIdentityProviderService.DescribeUserPoolClient");
        headerProperties.put("Content-Type", "application/x-amz-json-1.1");
        headerProperties.put("Content-Length", String.valueOf(body.length()));
        headerProperties.put("Host", "cognito-idp.eu-west-1.amazon.aws.com");
        headerProperties.put("X-Amz-Date", getFormattedTimestamp());
        String hashedPayload = calculateSHA256Hash(body);
        headerProperties.put("X-Amz-Content-Sha256", hashedPayload);
        Map<String, String> sortedHeaders = new TreeMap<>(headerProperties);


        String authorizationHeader = signRequest("POST", "cognito-idp", headerProperties, hashedPayload);
        sortedHeaders.put("Authorization", authorizationHeader);

        sortedHeaders = new TreeMap<>(sortedHeaders);

        return sortedHeaders;
    }

    static final String region = "eu-west-1";


    private static String signRequest(String method, String service, Map<String, String> requestHeaders, String body) {

        String canonicalRequest = buildCanonicalRequest(method, "/", requestHeaders, body);

        String algorithm = "AWS4-HMAC-SHA256";
        String requestTime = getFormattedTimestamp();
        String dateStamp = requestTime.substring(0, 8);

        String stringToSign = buildStringToSign(requestTime, dateStamp, canonicalRequest, service);
        byte[] signingKey = generateSigningKey(dateStamp, service);
        String signature = calculateHMAC(stringToSign, signingKey);

        return algorithm + " Credential= IAM_CLIENT_ID" + "/" + dateStamp + "/" + region + "/" + service + "/aws4_request, " +
                "SignedHeaders=" + getSignedHeaders(requestHeaders) + ", Signature=" + signature;
    }


    private static String buildCanonicalRequest(String method, String endpoint, Map<String, String> headers, String hashedBody) {

        // Normalize headers (lowercase and sort)
        Map<String, String> canonicalHeaders = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        canonicalHeaders.putAll(headers);


        // Construct canonical headers string
        StringBuilder canonicalHeadersStrBuilder = new StringBuilder();

        for (Map.Entry<String, String> header : canonicalHeaders.entrySet()) {
            if (!header.getKey().equals("")) {
                canonicalHeadersStrBuilder.append(header.getKey().toLowerCase()).append(':').append(header.getValue().trim()).append('\n');
            } else {
                canonicalHeadersStrBuilder.append(header.getKey().toLowerCase()).append(header.getValue().trim()).append('\n');
            }
        }
        String canonicalHeadersString = canonicalHeadersStrBuilder.substring(0, canonicalHeadersStrBuilder.toString().length() - 1);

        // Construct signed headers string
        String signedHeaders = getSignedHeaders(headers);
        String canonicalRequest = String.join("\n", method, endpoint, canonicalHeadersString, signedHeaders, calculateSHA256Hash("body"));
        return canonicalRequest;

    }

    private static String buildStringToSign(String requestTime, String dateStamp, String canonicalRequest, String service) {
        String algorithm = "AWS4-HMAC-SHA256";

        String stringToSign = String.join("\n",
                algorithm,
                requestTime,
                String.join("/", dateStamp, region, service, "aws4_request"),
                calculateSHA256Hash(canonicalRequest));
        return stringToSign;
    }


    private static byte[] generateSigningKey(String dateStamp, String service) {
        String secretId = ""; //keeping this blank for sake of security
        try {

            // Generate the key components
            byte[] secretKey = ("AWS4" + secretId).getBytes(StandardCharsets.UTF_8);
            byte[] dateKey = hmacSHA256(dateStamp, secretKey);
            byte[] regionKey = hmacSHA256(region, dateKey);
            byte[] serviceKey = hmacSHA256(service, regionKey);
            byte[] signingKey = hmacSHA256("aws4_request", serviceKey);

            return signingKey;
        } catch (Exception e) {
            throw new RuntimeException("Error generating signing key", e);
        }

    }

    private static String calculateHMAC(String data, byte[] key) {
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "HmacSHA256");
            mac.init(secretKeySpec);

            // Compute the HMAC
            byte[] hmacBytes = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));

            // Convert the result to a hexadecimal string
            StringBuilder result = new StringBuilder();
            for (byte b : hmacBytes) {
                result.append(String.format("%02x", b));
            }

            return result.toString();
        } catch (Exception e) {
            throw new RuntimeException("Error calculating HMAC", e);
        }
    }

    private static String getFormattedTimestamp() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(new Date());
    }

    private static String calculateSHA256Hash(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();

            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Error calculating SHA-256 hash", e);
        }
    }

    /**
     * returns which properties were signed.
     *
     * @param headers
     * @return
     */
    private static String getSignedHeaders(Map<String, String> headers) {
        Set<String> lowercaseKeySet = new TreeSet<>();
        for (String str : headers.keySet()) {
            if (!str.equals("")) {
                lowercaseKeySet.add(str.toLowerCase());
            }
        }

        return String.join(";", lowercaseKeySet);

    }


    private static byte[] hmacSHA256(String data, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "HmacSHA256");
        mac.init(secretKeySpec);
        return mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
    }
}
